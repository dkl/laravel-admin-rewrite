<?php
/**
 * User：liujun
 * Date：2024/5/11
 * Time：16:22
 */

namespace Encore\Admin\Auth\Database;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $type
 * @property string $name
 * @property string $help
 * @property string $slug
 * @property string $value
 * @property string $options
 * @property int $order
 * @property string $form_type
 * @property string $rules
 */
class Configuration extends Model
{

    public $timestamps = false;

    protected $fillable = [
        'type', 'name', 'help', 'slug', 'value', 'options', 'order', 'form_type','rules'
    ];

    const FORM_TYPES = [
        'text'           => '文本框',
        'switch'         => '开关',
        'textarea'       => '文本域',
        'radio'          => '单选框',
        'checkbox'       => '多选框',
        'select'         => '下拉框',
        'multipleSelect' => '多选下拉框',
    ];

    public function __construct(array $attributes = [])
    {
        $connection = config('admin.database.connection') ?: config('database.default');
        $this->setConnection($connection);
        $this->setTable(config('admin.database.configurations_table'));
        parent::__construct($attributes);
    }
}