<?php

namespace Encore\Admin\Grid\Tools;

use Illuminate\Database\Eloquent\Builder;

trait WithStatusFilterTrait
{
    /**
     * for table where has status column tab filter
     * @param Builder $builder
     * @return void
     */
    public function scopeStatusFilter(Builder $builder)
    {
        if ($status = request('status')) {
            $builder->where('status', $status);
        }
    }
}