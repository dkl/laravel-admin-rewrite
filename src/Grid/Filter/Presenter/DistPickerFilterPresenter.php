<?php
/**
 * User：liujun
 * Date：2022/3/4
 * Time：9:50 PM
 */
namespace Encore\Admin\Grid\Filter\Presenter;

class DistPickerFilterPresenter extends Presenter
{
    public function view() : string
    {
        return 'admin::filter.distPickerFilter';
    }
}