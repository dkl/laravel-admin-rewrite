<?php

namespace Encore\Admin\Grid\Displayers;

class Link extends AbstractDisplayer
{
    public function display($url = '', $column = 'id', $target = '_SELF'): string
    {
        $url = $url ?: request()->url();
        $id = data_get($this->row, $column);
        $url = url()->isValidUrl($url) ? $url : admin_base_path($url);
        return "<a href='$url/$id' target='$target'>{$this->value}</a>";
    }
}
