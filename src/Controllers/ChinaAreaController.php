<?php
/**
 * User：liujun
 * Date：2022/3/13
 * Time：4:59 PM
 */

namespace Encore\Admin\Controllers;

use Encore\Admin\Auth\Database\ChinaArea;
use Encore\Admin\Form;
use Encore\Admin\Grid;

class ChinaAreaController extends AdminController
{
    protected $title = '中国行政区';

    protected function grid()
    {
        $grid = new Grid(new ChinaArea());
        $grid->column('name', __('名称'))->width(320);
        $grid->column('parent.name', __('父级'))->width(280);
        $grid->column('code', __('行政编码'))->width(120);

        $grid->disableCreateButton()
            ->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete()->disableView();
            })->filter(function (Grid\Filter $filter) {
                $filter->disableIdFilter();
                $filter->column(6, function () use ($filter) {
                    $filter->like('name', '名称');
                });
                $filter->column(6, function () use ($filter) {
                    $filter->equal('code', '行政编码');
                });
            });
        return $grid;
    }

    protected function form(): Form
    {
        $form = new Form(new ChinaArea());

        $form->select('parent_id', __('父级'))->options(ChinaArea::parentOptions());
        $form->text('name', __('名称'))->rules('required|string|max:40');
        $form->text('code', __('行政编码'))->rules('required|integer|between:100000,999999');

        return $form;
    }
}