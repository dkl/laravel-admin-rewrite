<?php
/**
 * User：liujun
 * Date：2022/3/5
 * Time：7:53 PM
 */

namespace Encore\Admin\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UploadController
{
    /**
     * for wang editor upload images
     * @param Request $request
     * @return JsonResponse
     */
    public function wangEditorUpload(Request $request): JsonResponse
    {
        ini_set('max_execution_time', 0);
        $files = $request->file();
        $disk = \Storage::disk(config('admin.upload.disk'));

        $arr = [];
        foreach ($files as $file) {
            $filename = $disk->put('editor/' . date('Ym'), $file);
            $arr[] = $disk->url($filename);
        }
        return response()->json(['errno' => 0, 'data' => $arr]);
    }
}