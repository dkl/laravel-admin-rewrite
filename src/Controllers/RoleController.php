<?php

namespace Encore\Admin\Controllers;

use Encore\Admin\Auth\Database\Menu;
use Encore\Admin\Auth\Database\Permission;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Support\Str;

class RoleController extends AdminController
{
    private string $transFileName;

    public function __construct()
    {
        $this->transFileName = Str::singular(config('admin.database.roles_table'));
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function title()
    {
        return trans('admin.roles');
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $roleModel = config('admin.database.roles_model');

        $grid = new Grid(new $roleModel());
        $grid->model()->orderBy('id', 'desc');

        $grid->column('id', __("admin/$this->transFileName.id"))->sortable();
        $grid->column('slug', __("admin/$this->transFileName.slug"));
        $grid->column('name', __("admin/$this->transFileName.name"));

        $grid->column('permissions', __("admin/$this->transFileName.permissions"))
            ->count()
            ->suffix('种');

        $grid->column('created_at', trans('admin.created_at'))->sortable()->kind();
        $grid->column('updated_at', trans('admin.updated_at'))->sortable()->kind();

        $grid->actions(function (Grid\Displayers\Actions $actions) {
            $actions->disableView();
            $actions->disableDelete();
        });
        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();
            $filter->column(6, function () use ($filter) {
                $filter->where(function ($query) {
                    $query->where('name', 'like', "%{$this->input}%")
                        ->orWhere('slug', 'like', "%{$this->input}%");
                }, '名称标识')->placeholder('请输入名称名标识进行搜索');
            });
            $filter->column(6, function () use ($filter) {
                $filter->where(function ($query) {
                    $query->whereHas('permissions', function ($query) {
                        $query->where('id', $this->input);
                    });
                }, __("admin/$this->transFileName.permissions"))
                    ->select(Permission::query()->pluck('name', 'id'));
            });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id): Show
    {
        $roleModel = config('admin.database.roles_model');

        $show = new Show($roleModel::findOrFail($id));

        $show->field('id', 'ID');
        $show->field('slug', trans('admin.slug'));
        $show->field('name', trans('admin.name'));
        $show->field('permissions', trans('admin.permissions'))->as(function ($permission) {
            return $permission->pluck('name');
        })->label();
        $show->field('created_at', trans('admin.created_at'));
        $show->field('updated_at', trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form(): Form
    {
        $roleModel = config('admin.database.roles_model');
        $table = config('admin.database.roles_table');

        $form = new Form(new $roleModel());

        $form->text('slug', __("admin/$this->transFileName.slug"))
            ->icon('fa-tag')
            ->creationRules("required|max:32|unique:$table,slug")
            ->updateRules("required|max:32|unique:$table,slug,{{id}}");

        $form->text('name', __("admin/$this->transFileName.name"))
            ->creationRules("required|max:32|unique:$table,name")
            ->updateRules("required|max:32|unique:$table,name,{{id}}");

        $form->permissionChecker('permissions', __("admin/$this->transFileName.permissions"))
            ->options(
                Menu::where('parent_id', 0)
                    ->with(['permissions' => function ($query) {
                        $query->select('id', 'name', 'admin_menu_id')
                            ->orderBy('name');
                    }])
                    ->orderBy('order')
                    ->get(['id', 'title', 'icon'])
            );

        $form->tools(function (Form\Tools $tools) {
            $tools->disableView();
        });
        return $form;
    }
}
