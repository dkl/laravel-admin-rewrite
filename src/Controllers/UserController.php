<?php

namespace Encore\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends AdminController
{
    private string $transFileName;

    public function __construct()
    {
        $this->transFileName = Str::singular(config('admin.database.users_table'));
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function title(): string
    {
        return '系统用户';
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid(): Grid
    {
        $userModel = config('admin.database.users_model');

        $grid = new Grid(new $userModel());

        $grid->column('id', __("admin/$this->transFileName.id"))->sortable();
        $grid->column('username', __("admin/$this->transFileName.username"));
        $grid->column('name', __("admin/$this->transFileName.name"));
        $grid->column('email', __("admin/$this->transFileName.email"));
        $grid->column('roles', trans('admin.roles'))->pluck('name')->label();
        $grid->column('is_enabled',__("admin/$this->transFileName.is_enabled"))->bool();
        $grid->column('created_at', trans('admin.created_at'))->sortable()->kind();

        $grid->filter(function (Grid\Filter $filter){
            $filter->disableIdFilter();
            $filter->column(6, function () use ($filter) {
                $filter->equal('name',__("admin/$this->transFileName.name"));
                $filter->equal('username',__("admin/$this->transFileName.username"));
            });
            $filter->column(6, function () use ($filter) {
                $filter->where(function ($query){
                    $query->whereHas('roles',function ($query){
                        $query->where('id',$this->input);
                    });
                },'角色')->select(config('admin.database.roles_model')::pluck('name','id'));
               $filter->where(function ($query){
                    $query->whereHas('permissions',function ($query){
                        $query->where('id',$this->input);
                    });
                },'权限')->select(config('admin.database.permissions_model')::pluck('name','id'));
            });
        });

        $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableDelete();
        });

        $grid->tools(function (Grid\Tools $tools) {
            $tools->batch(function (Grid\Tools\BatchActions $actions) {
                $actions->disableDelete();
            });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id): Show
    {
        $userModel = config('admin.database.users_model');

        $show = new Show($userModel::findOrFail($id));

        $show->field('id', __("admin/$this->transFileName.id"));
        $show->field('username', __("admin/$this->transFileName.username"));
        $show->field('name',__("admin/$this->transFileName.name"));
        $show->field('avatar',__("admin/$this->transFileName.avatar"))->img();
        $show->field('is_enabled',__("admin/$this->transFileName.is_enabled"))->bool();
        $show->field('roles', trans('admin.roles'))->as(function ($roles) {
            return $roles->pluck('name');
        })->label();
        $show->field('permissions', trans('admin.permissions'))->as(function ($permission) {
            return $permission->pluck('name');
        })->label();
        $show->field('created_at', trans('admin.created_at'))->kind();
        $show->field('updated_at', trans('admin.updated_at'))->kind();

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    public function form(): Form
    {
        $userModel = config('admin.database.users_model');
        $permissionModel = config('admin.database.permissions_model');
        $roleModel = config('admin.database.roles_model');

        $form = new Form(new $userModel());

        $userTable = config('admin.database.users_table');
        $connection = config('admin.database.connection');

        $form->text('username', trans('admin.username'))
            ->creationRules(['required', "unique:{$connection}.{$userTable}"])
            ->updateRules(['required', "unique:{$connection}.{$userTable},username,{{id}}"]);

        $form->text('name',__("admin/$this->transFileName.name"))->rules('required')->icon('fa-user-circle-o');
        $form->email('email',__("admin/$this->transFileName.email"))->rules('required')->icon('fa-envelope-o');
        $form->image('avatar', trans('admin.avatar'))->uniqueName()->dir('avatars/'.date('ym'));
        $form->switch('is_enabled',__("admin/$this->transFileName.is_enabled"))->states();
        $form->password('password', trans('admin.password'))->rules('required|confirmed');
        $form->password('password_confirmation', trans('admin.password_confirmation'))->rules('required')
            ->default(function ($form) {
                return $form->model()->password;
            });

        $form->ignore(['password_confirmation']);

        $form->multipleSelect('roles', trans('admin.roles'))->options($roleModel::all()->pluck('name', 'id'));
        $form->multipleSelect('permissions', trans('admin.permissions'))->options($permissionModel::all()->pluck('name', 'id'));

        $form->saving(function (Form $form) {
            if ($form->password && $form->model()->password != $form->password) {
                $form->password = Hash::make($form->password);
            }
        });

        return $form;
    }
}
