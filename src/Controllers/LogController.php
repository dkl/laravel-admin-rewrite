<?php

namespace Encore\Admin\Controllers;

use Encore\Admin\Auth\Database\OperationLog;
use Encore\Admin\Grid;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class LogController extends AdminController
{
    private string $transFileName;

    public function __construct()
    {
        $this->transFileName = Str::singular(config('admin.database.operation_log_table'));
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function title()
    {
        return trans('admin.operation_log');
    }

    /**
     * @return Grid
     */
    protected function grid(): Grid
    {
        $grid = new Grid(new OperationLog());

        $grid->model()->orderBy('id', 'DESC');

        $grid->column('id', __("admin/$this->transFileName.id"))->sortable();
        $grid->column('user.name', __("admin/$this->transFileName.user_id"));
        $grid->column('method', __("admin/$this->transFileName.method"))->display(function ($method) {
            $color = Arr::get(OperationLog::$methodColors, $method, 'grey');

            return "<span class=\"badge bg-$color\">$method</span>";
        });
        $grid->column('path', __("admin/$this->transFileName.path"))->label('info');
        $grid->column('ip', __("admin/$this->transFileName.ip"))->label('primary');
        $grid->column('input', __("admin/$this->transFileName.input"))->display(function ($input) {
            $input = json_decode($input, true);
            $input = Arr::except($input, ['_pjax', '_token', '_method', '_previous_']);
            if (empty($input)) {
                return '<code>{}</code>';
            }
            return '<pre>' . json_encode($input, JSON_PRETTY_PRINT | JSON_HEX_TAG) . '</pre>';
        });

        $grid->column('created_at', __("admin/$this->transFileName.created_at"))->kind()->sortable();

        $grid->disableRowSelector(false)->disableActions();

        $grid->disableCreateButton();

        $grid->filter(function (Grid\Filter $filter) {
            $filter->disableIdFilter();

            $filter->column(6, function () use ($filter) {
                $userModel = config('admin.database.users_model');
                $filter->equal('user_id', __("admin/$this->transFileName.user_id"))->select($userModel::all()->pluck('name', 'id'));
                $filter->equal('method',__("admin/$this->transFileName.method"))->select(array_combine(OperationLog::$methods, OperationLog::$methods));
            });

            $filter->column(6, function () use ($filter) {
                $filter->like('path',__("admin/$this->transFileName.path"));
                $filter->equal('ip',__("admin/$this->transFileName.ip"));
            });
        });

        return $grid;
    }

    /**
     * @param mixed $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $ids = explode(',', $id);

        if (OperationLog::destroy(array_filter($ids))) {
            $data = [
                'status'  => true,
                'message' => trans('admin.delete_succeeded'),
            ];
        } else {
            $data = [
                'status'  => false,
                'message' => trans('admin.delete_failed'),
            ];
        }

        return response()->json($data);
    }
}
