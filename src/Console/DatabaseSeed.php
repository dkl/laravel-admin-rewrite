<?php

namespace Encore\Admin\Console;

use DB;
use Illuminate\Console\Command;

class DatabaseSeed extends Command
{
    protected $signature = 'admin:db_bak';

    protected $description = '使用iSeed备份数据库';


    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        if ($this->confirm('是否要开始备份所有数据表?')) {
            $database = config('database.connections.mysql.database');
            $tables = DB::select("SELECT TABLE_NAME FROM information_schema.tables where table_schema='$database'");
            $tables = collect($tables)->pluck('TABLE_NAME')->filter(function ($table) {
                return !in_array($table,['migrations','	admin_operation_log']);
            })->all();
            $str = implode(',', $tables);
            $this->call('iseed', [
                'tables' => $str,
                '--force' => 'default'
            ]);
            $this->info('数据备份完毕！');
        }
    }
}
