<?php

namespace Encore\Admin\Console;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;

class ExporterCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:exporter {name} {--name=} {--filename=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a exporter for the given model';


    protected function replaceClass($stub, $name): string
    {
        $stub = parent::replaceClass($stub, $name);
        $filename = $this->option('filename');
        return str_replace(
            [
                'DummyName',
                'DummyFileName',
            ],
            [
                $this->option('name') ?: $this->argument('name'),
                $filename ? $filename . '.xlsx' : $this->argument('name') . '.xlsx',
            ],
            $stub
        );
    }

    /**
     * 默认的命名空间.
     * @param $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace): string
    {
        $segments = explode('\\', config('admin.route.namespace'));
        array_pop($segments);
        array_push($segments, 'Actions', 'Exports');
        return implode('\\', $segments);
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub(): string
    {
        return __DIR__ . '/stubs/exporter.stub';
    }
}
