<?php

namespace Encore\Admin\Console;

use DB;
use Illuminate\Console\Command;

class DescTableSchema extends Command
{
    protected $signature = 'admin:desc {table : The name of the table to describe}';
    protected $description = 'Describe a specified database table like MySQL DESC command';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $table = $this->argument('table');

        if (!DB::table('information_schema.tables')->where('table_name', $table)->exists()) {
            $this->error("Table {$table} does not exist");
            return;
        }

        $columns = DB::select("SHOW FULL COLUMNS FROM {$table}");
        $headers = ['Field', 'Type', 'Null', 'Key', 'Default', 'Extra', 'Comment'];
        $rows = [];
        foreach ($columns as $column) {
            $rows[] = [
                $column->Field,
                $column->Type,
                $column->Null,
                $column->Key,
                $column->Default,
                $column->Extra,
                $column->Comment,
            ];
        }
        $this->table($headers, $rows);
    }
}
