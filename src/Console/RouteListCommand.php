<?php
/**
 * User：liuJun
 * Date：2022/3/4
 * Time：12:40 PM
 */

namespace Encore\Admin\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Route;

class RouteListCommand extends Command
{
    protected $signature = 'admin:route-list {--prefix=}';

    protected $description = '生成路由列表';

    public function handle()
    {
        $prefix = $this->option('prefix');

        collect(Route::getRoutes())
            ->filter(function ($route) use ($prefix) {
                if ($prefix) {
                    return Str::contains(data_get($route, 'action.prefix'), $prefix);
                }
                return true;
            })->map(function ($route) {
                $route =  sprintf(
                    '%s %s',
                    str_pad(data_get($route, 'methods.0'), 8),
                    $route->uri()
                );
                $this->info($route);
            });
    }
}