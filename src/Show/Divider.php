<?php

namespace Encore\Admin\Show;

class Divider extends Field
{
    public function render(): string
    {
        return <<<HTML
<div class="col col-sm-12 h5" style="background: #f5faff;padding: 15px 10px;margin: 2px 0">
     $this->name
</div>
HTML;
    }
}
