<?php

namespace Encore\Admin;

/**
 * 服务调用单例
 */
class Instance
{
    private static $instance;

    public static function getIns()
    {
        if (static::$instance instanceof static) {
            return static::$instance;
        }
        return static::$instance = new static();
    }

    private function __clone()
    {
    }
}