<?php
/**
 * User：liujun
 * Date：2023/7/10
 * Time：16:33
 */

namespace Encore\Admin\Form\Field;

use Encore\Admin\Form\Field;

class MarkdownEditor extends Field
{
    protected $view = 'admin::form.markdownEditor';

    protected static $css = [
        '/vendor/laravel-admin/editor-md-1.5.0/css/editormd.min.css',
    ];

    protected static $js = [
        '/vendor/laravel-admin/editor-md-1.5.0/js/editormd.js',
    ];

    protected string $libPath = '/vendor/laravel-admin/editor-md-1.5.0/lib/';

    protected int $height = 400;

    protected array $toolbarIcons = [
        'h2', 'h3', 'italic', 'quote', '|',
        'list-ul', 'list-ol', '|',
        'link', 'image', 'code-block', 'table', 'html-entities', 'pagebreak', '|',
        'watch', 'fullscreen', 'search', 'help', 'info',
    ];

    protected string $imageUploadURL = ''; // {success : 0 | 1, message : "提示的信息", url   : "图片地址"}

    /**
     * 设置编辑器高度
     * @param int $height
     * @return $this
     */
    public function setHeight(int $height): MarkdownEditor
    {
        $this->height = $height;
        return $this;
    }

    /**
     * 设置编辑器工具栏
     * @param array $toolbarIcons
     * @return $this
     */
    public function setToolbarIcons(array $toolbarIcons): MarkdownEditor
    {
        $this->toolbarIcons = $toolbarIcons;
        return $this;
    }

    /**
     * 设置编辑器图片上传地址
     * @param string $imageUploadURL
     * @return $this
     */
    public function setImageUploadURL(string $imageUploadURL): MarkdownEditor
    {
        $this->imageUploadURL = $imageUploadURL;
        return $this;
    }

    public function render()
    {
        $id = $this->formatName($this->id);

        $config = (array)config('admin.extensions.markdown-editor.config');

        $config = json_encode(array_merge([
            'width' => '100%',
            'height' => $this->height,
            'syncScrolling' => 'single',
            'path' => $this->libPath,
            'toolbarIcons' => $this->toolbarIcons,
            'saveHTMLToTextarea' => true,
            'placeholder' => '请输入内容...',
            'imageUpload' => true,
            'imageFormats' => ['jpg', 'jpeg', 'gif', 'png', 'bmp', 'webp'],
            'imageUploadURL' => admin_base_path('upload') . '?_token=' . csrf_token(),
        ], $config, $this->options));

        $this->script = <<<EOT
(function ($) {
    if($('#input-$id').attr('initialized')){
        return;
        }
     var markdownEditor = editormd("{$this->id}", {$config});   
     
     // 全屏时 设置z-index
    markdownEditor.on('fullscreen', function() {
           $('#{$this->id}').css({'zIndex': 9999});
    });
    
    // 退出全屏时 设置z-index
    markdownEditor.on('fullscreenExit', function() {
           $('#{$this->id}').css({'zIndex': 0});
    });
    
     // 隐藏编辑品中的 editormd-preview-close-btn
     $('.editormd-preview-close-btn').hide();
      
     // 在编辑器内容改变时，将内容同步到 隐藏的input中
     markdownEditor.on("change", function() {
         $('#input-$id').val(markdownEditor.getMarkdown());
     });
     
     $('#input-$id').attr('initialized',1);
})(jQuery);
EOT;
        return parent::render();
    }
}