<?php
/**
 * 权限checkbox表单
 * User：liujun
 * Date：2022/3/5
 * Time：5:21 PM
 */

namespace Encore\Admin\Form\Field;

use Encore\Admin\Form\Field;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany as HasManyRelation;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class PermissionChecker extends Field
{
    protected $otherKey;

    protected function getOtherKey()
    {
        if ($this->otherKey) {
            return $this->otherKey;
        }

        if (is_callable([$this->form->model(), $this->column])) {
            $relation = $this->form->model()->{$this->column}();

            if ($relation instanceof BelongsToMany) {
                /* @var BelongsToMany $relation */
                $fullKey = $relation->getQualifiedRelatedPivotKeyName();
                $fullKeyArray = explode('.', $fullKey);

                return $this->otherKey = 'pivot.'.end($fullKeyArray);
            } elseif ($relation instanceof HasManyRelation) {
                /* @var HasManyRelation $relation */
                return $this->otherKey = $relation->getRelated()->getKeyName();
            }
        }

        throw new \Exception('Column of this field must be a `BelongsToMany` or `HasMany` relation.');
    }

    /**
     * 编辑数据是触发
     * @param Model $data 角色数据
     * @return void
     * @throws \Exception
     */
    public function fill($data)
    {
        //key 表单字段名称
        if ($this->form && $this->form->shouldSnakeAttributes()) {
            $key = Str::snake($this->column);
        } else {
            $key = $this->column;
        }

        //角色权限
        $relations = Arr::get($data, $key);

        if (is_string($relations)) {
            $this->value = explode(',', $relations);
        }

        if (!is_array($relations)) {
            $this->applyCascadeConditions();

            return;
        }

        //最终提取权限ID
        $first = current($relations);
        if (is_null($first)) {
            $this->value = null;
            // MultipleSelect value store as an ont-to-many relationship.
        } elseif (is_array($first)) {
            foreach ($relations as $relation) {
                $this->value[] = Arr::get($relation, $this->getOtherKey());
            }
            // MultipleSelect value store as a column.
        } else {
            $this->value = $relations;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setOriginal($data)
    {
        $relations = Arr::get($data, $this->column);

        if (is_string($relations)) {
            $this->original = explode(',', $relations);
        }

        if (!is_array($relations)) {
            return;
        }

        $first = current($relations);

        if (is_null($first)) {
            $this->original = null;

            // MultipleSelect value store as an ont-to-many relationship.
        } elseif (is_array($first)) {
            foreach ($relations as $relation) {
                $this->original[] = Arr::get($relation, $this->getOtherKey());
            }

            // MultipleSelect value store as a column.
        } else {
            $this->original = $relations;
        }
    }

    public function render()
    {
        $this->addVariables([
            'options' => $this->options
        ]);
        return parent::render();
    }
}