<?php
/**
 * User：liujun
 * Date：2022/3/4
 * Time：10:01 PM
 */

namespace Encore\Admin\Form\Field;

use Encore\Admin\Form\Field;

class WangEditor extends Field
{
    protected $view = 'admin::form.wangEditor';

    protected static $css = [
        '/vendor/laravel-admin/wangEditor-3.0.10/release/wangEditor.css',
    ];

    protected static $js = [
        '/vendor/laravel-admin/wangEditor-3.0.10/release/wangEditor.js',
    ];

    public function render()
    {
        $id = $this->formatName($this->id);

        $config = (array) config('admin.extensions.wang-editor.config');

        $config = json_encode(array_merge([
            'zIndex'              => 0,
            'uploadImgShowBase64' => true,
        ], $config, $this->options));

        $token = csrf_token();

        $this->script = <<<EOT
(function ($) {

    if ($('#{$this->id}').attr('initialized')) {
        return;
    }

    var E = window.wangEditor
    var editor = new E('#{$this->id}');
    
    this.editor = editor;
    
    editor.customConfig.uploadImgParams = {_token: '$token'}
    
    Object.assign(editor.customConfig, {$config})
    
    editor.customConfig.onchange = function (html) {
        $('#input-$id').val(html);
    }
    editor.create();
    
    $('#{$this->id}').attr('initialized', 1);
})(jQuery);
EOT;
        return parent::render();
    }
}