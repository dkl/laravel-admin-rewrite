<?php

namespace Encore\Admin\Form\Field;

class Number extends Text
{
    protected static $js = [
        '/vendor/laravel-admin/number-input/bootstrap-number-input.js',
    ];

    public function render()
    {
        $this->default($this->default);

        $this->script = <<<EOT

$('{$this->getElementClassSelector()}:not(.initialized)')
    .addClass('initialized')
    .bootstrapNumber({
        upClass: 'default',
        downClass: 'default',
        center: true
    });

EOT;

        $this->prepend('')->defaultAttribute('style', 'width: 126px');

        return parent::render();
    }

    /**
     * Set min value of number field.
     *
     * @param int $value
     *
     * @return $this
     */
    public function min($value): Number
    {
        $this->attribute('min', $value);

        return $this;
    }

    /**
     * Set max value of number field.
     *
     * @param int $value
     *
     * @return $this
     */
    public function max($value): Number
    {
        $this->attribute('max', $value);

        return $this;
    }
}
