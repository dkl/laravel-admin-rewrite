<?php
/**
 * 批量设置权限菜单
 * User：liujun
 * Date：2022/3/5
 * Time：11:16 AM
 */

namespace Encore\Admin\Actions\Custom;

use Encore\Admin\Actions\BatchAction;
use Encore\Admin\Actions\Response;
use Encore\Admin\Auth\Database\Permission;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class PermissionMenuBatchSetAction extends BatchAction
{
    protected $selector = '.permission-menu-batch-set-action';
    public $name = '设置菜单';

    public function handle(Collection $collection, Request $request): Response
    {
        Permission::query()->whereIn('id', $collection->pluck('id')->toArray())->update([
            'admin_menu_id' => $request->get('admin_menu_id'),
        ]);
        return $this->response()->success('权限菜单批量设置成功')->refresh();
    }

    public function form()
    {
        $this->select('admin_menu_id', __('admin/admin_permission.admin_menu_id'))
            ->options(config('admin.database.menu_model')::parentOptions())
            ->rules('required');
    }

}