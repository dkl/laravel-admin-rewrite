<?php
/**
 * 导入路由权限
 * User：liujun
 * Date：2022/3/5
 * Time：1:58 PM
 */

namespace Encore\Admin\Actions\Custom;

use Encore\Admin\Actions\Action;
use Encore\Admin\Actions\Response;

class ImportRoutePermissionAction extends Action
{
    protected $selector = '.import-route-permission-action';

    public function handle(): Response
    {
        $service = new AdminRouteService();
        $service->save();
        return $this->response()->success('路由权限导入成功')->refresh();
    }

    public function dialog()
    {
        $this->confirm('确认要批量导入路由权限吗？');
    }

    public function html(): string
    {
        return '<a href="javascript:;" class="btn btn-sm bg-purple import-route-permission-action">导入路由</a>';
    }
}