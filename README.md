# Laravel Admin Rewrite

### Gitee 地址

- https://gitee.com/dkl/laravel-admin-rewrite.git

### 安装shenheishe/laravel-admin

```bash
composer require shenheishe/laravel-admin --dev
```

- 运行下面的命令来发布资源：

```bash
php artisan vendor:publish --provider="Encore\Admin\AdminServiceProvider"
```

- 然后运行下面的命令完成安装：

```bash
php artisan admin:install
```

### Gird Column扩展显示

- price() 显示价格
- strong() 显示加粗
- number() 显示数字
- img() 显示单图或多图
- labels() 显示label多钟样式标签
- badges() 显示多种样式徽章
- kind() 日期友好显示
- onlyListTable() 隐藏工具栏与分页
- 新增导出功能 encore/laravel-admin/src/Grid/Exporters/AdminExporter.php

```php
 $exporter = (new Grid\Exporters\AdminExporter())
            ->withColumns([
                'id' => '会员ID',
            ])
            ->withFileName('会员列表')
            ->withRewrites([
                'status' => User::$statuses,
            ]);
```

### Show Field扩展显示

- bool() 显示布尔值
- number() 显示数字
- strong() 显示加粗
- price() 显示价格
- kind() 日期友好显示
- color() 显示自定义颜色
- fileDownload() 下载文件
- img() 显示单图或多图

### Filter 扩展

- boolean() 布尔值搜索
- yearMonth() 年月搜索 YYYY-MM 调用方式 where()->yearMonth()
- 自定义日期或时间范围搜索

```php
$filter->where(function (){
    $start = $this->input['start'];
    $end = $this->input['end'];
    //do something here
},'时间范围')->dateRange('YYYY-MM-DD');
```

### 增加admin命令

- admin:zw 生成表中文翻译文件
- admin:col generate all kinks of data from database tables
    - str|array|column|comment|gird|show|form|json|col
- admin:service 生成服务文件 
- admin:test 在tests/Feature目录下生成测试用例

```bash
php artisan admin:test Api --name=UserControler
php artisan admin:test Api --all

-- all 代表所有app/Http/Controllers下的所有文件
-- name 指定文件名
-- force 强制覆盖
```
- admin:route-list  生成路由列表
```bash
php artisan admin:route-list --prefix=api  
```

### 新增distPicker省市区选择

```php
表单中使用
$form->distPicker(['province_id' => '省', 'city_id' => '市', 'district_id' => '区'], '区域选择')
     ->default(['province' => '', 'city' => '', 'district' => ''])
     ->rules('required');
搜索中使用
$filter->distPicker('province_id', 'city_id', 'district_id', '地域选择');
```

### 新增wangEditor富文本编辑器

```php
$form->wangEditor('content','内容')->rules('required|max:1000');
```

### 置入异常日志文件

- /admin/error/logs

### 一键导入app/admin/actions下所有Action至权限数据表

- 在配置文件中admin.php新增权限开关 admin.action_permissions.enable
- 注意 先建好菜单再执行导入动作 这样可以更好的匹配动作权限权限组

### 根据routes.php中的路由自动生成权限

- 自动分组
- 自动匹配名称 (根据菜单中的uri字段匹配)
- 注意 先保证菜单及routes.php文件已经都存在的情况下执行

### 增加角色中按组选择权限表单

- admin_permissions 表中新增admin_menu_id字段

```php
$form->permissionChecker('permissions', __('admin/admin_role.permissions'))
        ->options(
        Menu::where('parent_id', 0)
            ->with('permissions:id,admin_menu_id,name')
            ->orderBy('order')
            ->get(['id', 'title','icon'])
       );
```

### 增加StatusFilterTab选项卡功能

- 文件位置 encore/laravel-admin/src/Grid/Tools/StatusFilterTab.php
- 视图位置 encore/laravel-admin/resources/views/grid/actions/statusfiltertab.blade.php
- 调用方式
    - Model use Encore\Admin\Grid\Tools\WithStatusFilterTrait;

```php
列表中调用
 $grid->model()->statusFilter();
 
 $grid->tools(function (Grid\Tools $tools){
    $tools->append(new Grid\Tools\StatusFilterTab(new User()));
});
```

### 更新

- 菜单经过权限过滤后无子菜时将会自动隐藏父级菜单

```javascript
 //if all child menu can't be visible, hide parent menu
$('li.treeview a').each(function () {
    if ($(this).attr('href') === '#') {
        if ($(this).next('ul').text().replace(/\s+/g, '').length === 0) {
            $(this).parent('li').addClass('hidden');
        }
    }
});
```