<style>
    .detail-style .form-group {
        margin-bottom: 0 !important;
    }

    .detail-style .form-group {
        margin: 0 !important;
    }

    .detail-style .form-group div.form-row {
        padding: 5px 0;
    }

    .detail-style div.form-group:nth-child(even) {
        background: #f8f8f8;
    }

    .detail-style .form-group label, .detail-style .form-group .content-col {
        text-align: left;
        font-weight: normal;
        padding: 5px 10px;
    }

    .detail-style .box {
        background: none;
        border: none !important;
        margin-bottom: 0 !important;
    }

    .detail-style .box-body {
        padding: 0 !important;
    }
</style>
<div class="box box-{{ $style }}">
    <div class="box-header with-border" style="padding: 15px">
        <h3 class="box-title">{{ $title }}</h3>

        <div class="box-tools" style="top: 10px">
            {!! $tools !!}
        </div>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <div class="form-horizontal">

        <div class="box-body" style="padding: 0">

            <div class="fields-group detail-style">
                @foreach($fields as $field)
                    {!! $field->render() !!}
                @endforeach
            </div>

        </div>
        <!-- /.box-body -->
    </div>
</div>
