<div class="form-group">
    <div class="col-lg-12 form-row">
        <label class="col-sm-{{$width['label']}} control-label">{{ $label }}</label>
        <div class="col-sm-{{$width['field']}} content-col">
            @if($wrapped)
                <div class="box box-solid box-default no-margin box-show">
                    <!-- /.box-header -->
                    <div class="box-body">
                        @if($escape)
                            {{ $content }}
                        @else
                            {!! $content !!}
                        @endif
                    </div><!-- /.box-body -->
                </div>
            @else
                @if($escape)
                    {{ $content }}
                @else
                    {!! $content !!}
                @endif
            @endif
        </div>
    </div>
</div>