<style>
    .actionsTable .col-sm-12 {
        padding: 0 !important;
    }

    .actionsTable .input-group span.input-group-addon {
        display: none !important;
    }
</style>

<div class="row">
    <label class="{{$viewClass['label']}}" style="width: 100%"> {{ $label }}</label>
    <div class="{{$viewClass['field']}}" style="width: 100%;margin-top:8px;">
        <div id="has-many-{{$column}}" class="actionsTable">
            <table class="table table-has-many has-many-{{$column}} table-striped table-bordered">
                <thead>
                <tr>
                    @foreach($headers as $header)
                        <th>{{ $header }}</th>
                    @endforeach

                    <th class="hidden"></th>

                    @if($options['allowDelete'])
                        <th></th>
                    @endif
                </tr>
                </thead>
                <tbody class="has-many-{{$column}}-forms">
                @foreach($forms as $pk => $form)
                    <tr class="has-many-{{$column}}-form fields-group">

                            <?php $hidden = ''; ?>

                        @foreach($form->fields() as $field)

                            @if (is_a($field, \Encore\Admin\Form\Field\Hidden::class))
                                    <?php $hidden .= $field->render(); ?>
                                @continue
                            @endif

                            <td>{!! $field->setLabelClass(['hidden'])->setWidth(12, 0)->render() !!}</td>
                        @endforeach

                        <td class="hidden">{!! $hidden !!}</td>

                        @if($options['allowDelete'])
                            <td class="form-group">
                                <div>
                                    <div class="remove btn btn-danger btn-sm pull-right"><i class="fa fa-times">&nbsp;</i></div>
                                </div>
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>

            <template class="{{$column}}-tpl">
                <tr class="has-many-{{$column}}-form fields-group">

                    {!! $template !!}

                    <td class="form-group">
                        <div>
                            <div class="remove btn btn-danger btn-sm pull-right"><i class="fa fa-times"></i></div>
                        </div>
                    </td>
                </tr>
            </template>

            @if($options['allowCreate'])
                <div class="form-group margin-bottom-none">
                    <div class="add btn btn-success btn-sm"><i class="fa fa-plus-circle"></i>&nbsp;{{ trans('admin.new') }}</div>
                </div>
            @endif
        </div>
    </div>
</div>
