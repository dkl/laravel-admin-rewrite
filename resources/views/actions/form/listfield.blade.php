@php($listErrorKey = "$column.values")

<div class="form-group">
    <label>{{$label}}</label>
    <div>
        <table class="table table-hover">

            <tbody class="list-{{$column}}-table">

            @foreach(old("{$column}.values", ($value ?: [])) as $k => $v)

                @php($itemErrorKey = "{$column}.values.{$loop->index}")

                <tr>
                    <td>
                        <div class="form-group {{ $errors->has($itemErrorKey) ? 'has-error' : '' }}">
                            <input name="{{ $column }}[values][]" value="{{ old("{$column}.values.{$k}", $v) }}"
                                   class="form-control"/>
                            @if($errors->has($itemErrorKey))
                                @foreach($errors->get($itemErrorKey) as $message)
                                    <label class="control-label" for="inputError"><i
                                            class="fa fa-times-circle-o"></i> {{$message}}</label><br/>
                                @endforeach
                            @endif
                        </div>
                    </td>

                    <td style="width: 75px;">
                        <div class="{{$column}}-remove btn btn-danger btn-sm pull-right">
                            <i class="fa fa-trash">&nbsp;</i>{{ __('admin.remove') }}
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td></td>
                <td>
                    <div class="{{ $column }}-add btn btn-success btn-sm pull-right">
                        <i class="fa fa-plus-circle"></i>&nbsp;{{ __('admin.new') }}
                    </div>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <template class="{{$column}}-tpl">
        <tr>
            <td>
                <div class="form-group">
                    <input name="{{ $column }}[values][]" class="form-control"/>
                </div>
            </td>

            <td style="width: 75px;">
                <div class="{{$column}}-remove btn btn-danger btn-sm pull-right">
                    <i class="fa fa-trash">&nbsp;</i>{{ __('admin.remove') }}
                </div>
            </td>
        </tr>
    </template>
</div>
