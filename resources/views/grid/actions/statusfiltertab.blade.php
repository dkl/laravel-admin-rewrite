<div class="btn-group btn-group-sm hidden-xs" data-toggle="buttons">
    @foreach($options as $option => $label)
        <label class="btn btn-sm {{ (string)\Request::get($statusField, 'all') === (string)$option ? 'active bg-navy' : 'btn-default' }}">
            <input type="radio" class='sFilter' value="{{ $option }}">{{$label}}
        </label>
    @endforeach
</div>

<div class="visible-xs pull-right" style="margin-left: 10px">
<select class="statusFilter form-control form-control-static input-sm">
    @foreach($options as $option => $label)
        <option {{ (string)\Request::get($statusField, 'all') === (string)$option ? 'selected' :'' }} value="{{$option}}">{{$label}}</option>
    @endforeach
</select>
</div>