<li>
    <a href="javascript:void(0);" class="nav-fullscreen">
        <i class="fa fa-arrows-alt"></i>
    </a>
</li>
<script>
    function toggleFullscreen(element) {
        if (!document.fullscreenElement &&    // Standard
            !document.webkitFullscreenElement && // Safari
            !document.mozFullScreenElement && // Older Firefox
            !document.msFullscreenElement) {  // IE/Edge
            if (element.requestFullscreen) {
                element.requestFullscreen();
            } else if (element.webkitRequestFullscreen) { // Safari
                element.webkitRequestFullscreen();
            } else if (element.mozRequestFullScreen) { // Older Firefox
                element.mozRequestFullScreen();
            } else if (element.msRequestFullscreen) { // IE/Edge
                element.msRequestFullscreen();
            }
        } else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) { // Safari
                document.webkitExitFullscreen();
            } else if (document.mozCancelFullScreen) { // Older Firefox
                document.mozCancelFullScreen();
            } else if (document.msExitFullscreen) { // IE/Edge
                document.msExitFullscreen();
            }
        }
    }
    $('.nav-fullscreen').click(function () {
        toggleFullscreen(document.body);
    });
</script>
