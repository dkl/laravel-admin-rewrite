<div class="{{$viewClass['form-group']}} {!! !$errors->has($errorKey) ? '' : 'has-error' !!}">

    <label for="{{$id}}" class="{{$viewClass['label']}} control-label">{{$label}}</label>

    <div class="{{$viewClass['field']}}">

        @include('admin::form.error')

        <div id="{{$id}}">
            <textarea style="display: none">{!! old($column),$value !!}</textarea>
        </div>

        <input id="input-{{$id}}" type="hidden" name="{{$name}}" value="{{ old($column, $value) }}"/>

        @include('admin::form.help-block')

    </div>
</div>
