<div class="tab-content fields-group" style="padding: 0 15px">
    @foreach($tabObj->getTabs() as $tab)
        <div class="tab-pane {{ $tab['active'] ? 'active' : '' }}" id="tab-{{ $tab['id'] }}">
            @foreach($tab['fields'] as $field)
                {!! $field->render() !!}
            @endforeach
        </div>
    @endforeach
</div>
