<style>
    label.checkbox-inline {
        width: 150px;
        margin-left: 5px !important;
    }
</style>
<div class="{{$viewClass['form-group']}} {!! !$errors->has($errorKey) ? '' : 'has-error' !!}">

    <label for="{{$id}}" class="{{$viewClass['label']}} control-label">{{$label}}</label>

    <div class="{{$viewClass['field']}} permission-box">

        @include('admin::form.error')

        <table class="table table-striped table-bordered table-hover" style="margin-bottom: -10px">
            <thead>
            <tr>
                <th>权限组模块</th>
                <th>功能权限</th>
            </tr>
            </thead>
            <tbody>
            @foreach($options as $menu)
                <tr>
                    <td style="vertical-align: middle">
                        <label class="checkbox-inline" style="width: 130px">
                            <i class="fa {{$menu['icon']}} pull-right text-gray"></i>
                            <input type="checkbox" class="check-all"/> <strong>{{$menu['title']}}</strong>
                        </label>
                    </td>
                    <td>
                        @foreach($menu['permissions'] as $per)
                            <label class="checkbox-inline">
                                <input type="checkbox" @if(in_array($per['id'],(array)old($name,$value))) checked @endif name="{{$name}}[]" value="{{$per['id']}}"/> {{$per['name']}}
                            </label>
                        @endforeach
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @include('admin::form.help-block')

    </div>
</div>
<script>
    $(function () {
        $('.check-all').each(function () {
            $(this).on('click', function () {
                let $children = $(this).parent().parent().parent().find("input[name='{{$name}}[]']");
                $children.prop("checked", $(this).is(":checked"));
            })
        });
    });
</script>