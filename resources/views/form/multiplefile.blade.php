<div class="{{$viewClass['form-group']}} {!! !$errors->has($errorKey.'*') ? '' : 'has-error' !!}">
    <label for="{{$id}}" class="{{$viewClass['label']}} control-label">{{$label}}</label>
    <div class="{{$viewClass['field']}}">
        @if($errors->has($errorKey.'*'))
            @foreach($errors->get($errorKey.'*') as $key => $messages)
                @if(is_array($messages))
                    @foreach($messages as $message)
                        <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>{{$message}}</label><br>
                    @endforeach
                @else
                    <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{$message}}</label>
                @endif
            @endforeach
        @endif
        <input type="file" class="{{$class}}" name="{{$name}}[]" {!! $attributes !!} />
        @isset($sortable)
            <input type="hidden" class="{{$class}}_sort" name="{{ $sort_flag."[$name]" }}"/>
        @endisset

        @include('admin::form.help-block')

    </div>
</div>