<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminTables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function getConnection()
    {
        return config('admin.database.connection') ?: config('database.default');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('admin.database.users_table'), function (Blueprint $table) {
            $table->id()->from(time())->comment('用户ID');
            $table->string('username', 32)->unique()->comment('用户名');
            $table->string('password', 60)->comment('密码');
            $table->string('name', 32)->comment('姓名');
            $table->string('email', 60)->nullable()->comment('邮箱');
            $table->string('avatar')->nullable()->comment('头像');
            $table->boolean('is_enabled')->nullable()->default(1)->comment('是否启用');
            $table->string('remember_token', 100)->nullable();
            $table->timestamps();
        });

        Schema::create(config('admin.database.roles_table'), function (Blueprint $table) {
            $table->increments('id')->comment('角色ID')->from(10000);
            $table->string('name', 50)->unique()->comment('角色名称');
            $table->string('slug', 50)->unique()->comment('角色标识');
            $table->timestamps();
        });

        Schema::create(config('admin.database.permissions_table'), function (Blueprint $table) {
            $table->increments('id')->comment('权限ID')->from(50000);
            $table->foreignId('admin_menu_id')->nullable()->index()->comment('权限组');
            $table->string('name', 50)->unique()->comment('权限名称');
            $table->string('slug', 64)->unique()->comment('权限标识');
            $table->string('http_method')->nullable()->comment('HTTP方法');
            $table->text('http_path')->nullable()->comment('HTTP路径');
            $table->timestamps();
        });

        Schema::create(config('admin.database.menu_table'), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->default(0)->comment('父级菜单');
            $table->integer('order')->default(0)->comment('排序');
            $table->string('title', 50)->comment('标题');
            $table->string('icon', 50)->comment('图标');
            $table->string('uri')->nullable()->comment('路径');
            $table->string('permission')->nullable()->comment('权限');

            $table->timestamps();
        });

        Schema::create(config('admin.database.role_users_table'), function (Blueprint $table) {
            $table->integer('role_id');
            $table->integer('user_id');
            $table->index(['role_id', 'user_id']);
            $table->timestamps();
        });

        Schema::create(config('admin.database.role_permissions_table'), function (Blueprint $table) {
            $table->integer('role_id');
            $table->integer('permission_id');
            $table->index(['role_id', 'permission_id']);
            $table->timestamps();
        });

        Schema::create(config('admin.database.user_permissions_table'), function (Blueprint $table) {
            $table->integer('user_id');
            $table->integer('permission_id');
            $table->index(['user_id', 'permission_id']);
            $table->timestamps();
        });

        Schema::create(config('admin.database.role_menu_table'), function (Blueprint $table) {
            $table->integer('role_id');
            $table->integer('menu_id');
            $table->index(['role_id', 'menu_id']);
            $table->timestamps();
        });

        Schema::create(config('admin.database.operation_log_table'), function (Blueprint $table) {
            $table->increments('id');
            $table->foreignId('user_id')->index()->comment('系统用户');
            $table->string('path')->comment('路径');
            $table->string('method', 10)->comment('HTTP方法');
            $table->string('ip')->comment('访问IP');
            $table->text('input')->comment('请求参数');
            $table->timestamps();
        });

        Schema::create('china_areas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('parent_id')->comment('父级');
            $table->string('name', 32)->comment('名称');
            $table->string('code', 6)->index()->comment('区域编码');
            $table->timestamps();
        });

        Schema::create(config('admin.database.configurations_table'), function (Blueprint $table) {
            $table->id();
            $table->string('type', 32)->nullable()->default('base')->comment('配置类型');
            $table->string('name', 32)->comment('配置名称');
            $table->string('help')->nullable()->comment('配置帮助');
            $table->string('slug', 64)->unique()->comment('配置标识');
            $table->string('form_type', 32)->nullable()->default('text')->comment('表单类型');
            $table->text('value')->nullable()->comment('配置值');
            $table->text('options')->nullable()->comment('配置选项');
            $table->integer('order')->default(0)->comment('排序');
            $table->string('rules')->nullable()->comment('验证规则');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(config('admin.database.users_table'));
        Schema::dropIfExists(config('admin.database.roles_table'));
        Schema::dropIfExists(config('admin.database.permissions_table'));
        Schema::dropIfExists(config('admin.database.menu_table'));
        Schema::dropIfExists(config('admin.database.user_permissions_table'));
        Schema::dropIfExists(config('admin.database.role_users_table'));
        Schema::dropIfExists(config('admin.database.role_permissions_table'));
        Schema::dropIfExists(config('admin.database.role_menu_table'));
        Schema::dropIfExists(config('admin.database.operation_log_table'));
        Schema::dropIfExists(config('admin.database.configurations_table'));
        Schema::dropIfExists('china_areas');
    }
}
